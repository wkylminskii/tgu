## Формирование базы данных выданных разрешений на осуществление деятельности по перевозке пассажиров и багажа легковым такси по субъектам Российской Федерации.


Процесс формирования общей таблицы включает:
- Получение данных по каждому региону
- Предобработка каждой таблицы по отдельности
- Унификация - приведение таблиц к единому виду
- Объединение результатов обработки в общую таблицу
